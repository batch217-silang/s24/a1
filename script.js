// console.log("Hello World!");
let number = Math.pow(3, 3);
console.log(`The cube of 3 is ${number}`);




const fullAddress = {
	houseNumber: 258, 
	street: "Washington Ave NW", 
	state: "California", 
	zipCode: 90011};

const {houseNumber, street, state, zipCode} = fullAddress;
console.log(`I live at ${houseNumber} ${street} , ${state} ${zipCode}`);



const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: 1075,
	height: 20,
	width: 3
};
const {name, species, weight, height, width} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} kgs with a measurement of ${height} ft ${width} in.`);


// numbers = forEach
const numbers = [1, 2, 3, 4, 5, 15];
numbers.forEach((number) =>{
	console.log(`${number}`);
});


class Dog {
	constructor(name, age, breed){
		this.name = name,
		this.age = age,
		this.breed = breed
	}
}
const myDog = new Dog('Khaleesi', '2', 'Husky');
console.log(myDog);